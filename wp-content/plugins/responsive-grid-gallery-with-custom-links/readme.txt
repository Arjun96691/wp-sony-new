=== Responsive Grid Gallery with Custom Links ===
Author URI: https://www.abcfolio.com
Plugin URI: https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-custom-links/
Contributors: abcfolio
Tags: custom links, grid gallery, image links, link images, responsive
Requires at least: 4.9
Tested up to: 5.7
Stable tag: 0.2.2
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Responsive Grid Gallery with Custom Links: Responsive grid of images. Each image can have a custom hyperlinks.

== Description ==

**Responsive Grid Gallery with Custom Links** - Grid gallery of images to be linked to other web pages. An easy way to add custom hyperlinks to images. Free plugin to showcase products, services, art or crafts.

= Live Preview =
[https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links-live-previews/](https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links-live-previews/).


= Features =
* Compatible with Gutenberg.
* Responsive layout.
* Easy to add to any page or post - just insert a shortcode.
* You can add captions and custom links to all images.
* Custom styles to create uniform look and feel that matches your theme.
* [Full list of features.](https://abcfolio.com/responsive-grid-gallery-with-custom-links-get-plugin/)


= Quick Start =
[https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links-quick-start/](https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links-quick-start/).

= User Guide =
Full documentation: [https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links/](https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links/).


= Premium Version Features =

* You can have up to 10 captions: hyperlinks, email, long description, HTML, horizontal lines.
* Grid can have to 12 columns. Free version 3 columns.
* Categories.
* Isotope galleries.
* Overlay and other on hover effects.


[Responsive Grid Gallery with Custom Links Pro](https://abcfolio.com/responsive-grid-gallery-with-custom-links-get-plugin/)


== Installation ==
Installation Instructions: [https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links-installation/](https://abcfolio.com/wordpress-plugin-responsive-grid-gallery-with-custom-links-installation/).


== Changelog ==

= 0.2.3 2021
* Update: abcf-html.php to version 171

= 0.2.2 20210320 =
* Update: Added session storage to options tabs.
* Update: library/abcfl-admin.css to version 144
* Update: library/abcfl-input.php to version 224
* Update: library/abcfl-mbox-save.php to version 115
* Update: library/abcfl-util.php to version 134

= 0.2.1 20210301 =
* Update: Tested with WordPress 5.7

= 0.2.0 20180416 =
* Update: Added fields Alt and Link Attributes.
* Update: Added new allowed HTML tags (library file).
* Update: Better handling of image options (image new functions).
* Update: Library files.

= 0.1.1 20170311 =
* Fix: Missing labels and constants.
* Fix: NT prefix.
* Fix: Items sort table. Fixed rendering issues in Chrome.

= 0.1.0 20170307 =
* New: Added custom roles. Min permissions Editor role.
* New: Users can create custom roles.
* New: Default template option.
* Update: Modified interface.
* Update: Libraries.
* Update: Added a few options for better upgrade path to Pro.
* Update: Removed unused scripts.

= 0.0.9 20160622 =
* Fix: Added missing closing DIV when row is not full.

= 0.0.8 20160519 =
* Fix: Plugin reloaded to fix error: No valid plugins were found.

= 0.0.7 20160518 =
* Update: Changed min permissions to Editor role.

= 0.0.6 20160427 =
* New: Added option to center the main container.
* Update: Added nonce to ajax calls.
* Fix: Function grid_item_txt_cntr_max_w OK index error

= 0.0.5 20160414 =
* Initial version.