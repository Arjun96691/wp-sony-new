<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-sony' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'v/(e`-36T&qqAxv]bz!t)ZWqV@bQ44kO+yC)?Zc_f29- Kytc2(FkHg/#|Bkqt@#' );
define( 'SECURE_AUTH_KEY',  '~D|v=VWy?H 7vgN8CM!~&;~#3D,EJ%1Zz;?Sl!BK2Dr:>PH,R>ak0yA0Xz6p$qUH' );
define( 'LOGGED_IN_KEY',    '`h1a`7I:(v><k]CP)gJ|N3NzkksL~w#W$j8u.f+Fu8}uYFNC5352?EKyfNULO||g' );
define( 'NONCE_KEY',        'pOxi9vpjW!hkkQ(w(nVkviXY`JvghHHX0*3!pHuAYvon3],{8[TDW+KSurO`},FB' );
define( 'AUTH_SALT',        'vjaGE{~!.<cZe,7M=zPx&Oo@<Bm6&3vO@%FjBO* !.FFm!.4&uC;%6rTD_#SW1$M' );
define( 'SECURE_AUTH_SALT', 'A&}Sko&1CJper)lxN}>{En-~wwgloe9aDwwvl(%tt.h,W[nO<MSe-C]}TDYyYyIr' );
define( 'LOGGED_IN_SALT',   '*6Ie n!MC?oWsMaI0B4wM2FUp6i3yKfS_FJ0G!i9}7U#6`](BO23bL|L;wGT-W:g' );
define( 'NONCE_SALT',       'f`:1GN[VTt|#tA?D?x=`/VERE.gYR#r5y/B8;8xHU.uA^1P7<Wqdm?FGi5RP{ty+' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
